const btn = document.getElementById('btn');
const btnBack = btn.querySelector('.btn-back');
const btnUpload = btn.querySelector('.btn-upload');
const btnShoots = btn.querySelector('.btn-shoot');
const btnNext = btn.querySelector('.btn-next');
const btnReplay = btn.querySelector('.btn-replay');
const content1 = document.getElementById('content1');
const title = content1.querySelector('title')
const inpFile = document.getElementById('inpFile');
const previewContainer = document.getElementById('imagePreview');
const previewImage = previewContainer.querySelector('.image-preview__image');
const previewDefaultText = previewContainer.querySelector('.image-preview__default-text');
inpFile.addEventListener('change', function(event) {
    const imageBase64 = [];
    const file = event.target.files[0];
    
    if(file) {
        const reader = new FileReader();
        previewDefaultText.style.display="none";
        previewImage.style.display="block";
        btnNext.style.display="block";
        btnUpload.style.display="none"
        btnShoots.style.display="none"
        

        reader.onload = function(e) {
            e.preventDefault()
            previewImage.setAttribute('src', e.target.result);
            imageBase64.push(e.target.result)
        };
        reader.readAsDataURL(file)
        console.log(imageBase64)
        btnNext.addEventListener('click', function(event) {
            console.log(this)
            title.innerHTML = "Ảnh Thẻ Căn Cước Mặt Sau"
        }) 
    }
});
